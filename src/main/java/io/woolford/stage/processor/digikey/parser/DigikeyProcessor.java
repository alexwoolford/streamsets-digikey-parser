/*
 * Copyright 2017 StreamSets Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.woolford.stage.processor.digikey.parser;

import com.streamsets.pipeline.api.Field;
import io.woolford.stage.lib.sample.Errors;

import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.api.StageException;
import com.streamsets.pipeline.api.base.SingleLaneRecordProcessor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class DigikeyProcessor extends SingleLaneRecordProcessor {

    /**
     * Gives access to the UI configuration of the stage provided by the {@link DigikeyDProcessor} class.
     */
    public abstract String getConfig();

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ConfigIssue> init() {
        // Validate configuration values and open any required resources.
        List<ConfigIssue> issues = super.init();

        if (getConfig().equals("invalidValue")) {
            issues.add(
                    getContext().createConfigIssue(
                            Groups.DIGIKEY.name(), "config", Errors.SAMPLE_00, "Here's what's wrong..."
                    )
            );
        }

        // If issues is not empty, the UI will inform the user of each configuration issue in the list.
        return issues;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
        // Clean up any open resources.
        super.destroy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void process(Record record, SingleLaneBatchMaker batchMaker) throws StageException {

        try {

            String html = String.valueOf(record.get("/pageSource").getValue());

            Document doc = Jsoup.parse(html);

            Element productTable = doc.getElementById("productTable").getElementById("lnkPart");

            Elements productTableRows = productTable.getElementsByTag("tr");

            for (Element productTableRow : productTableRows) {

                String dpn = productTableRow.getElementsByClass("tr-dkPartNumber").text();
                String mpn = productTableRow.getElementsByClass("tr-mfgPartNumber").text();
                String mfg = productTableRow.getElementsByClass("tr-vendor").text();
                String qtyAvailableRaw = productTableRow.getElementsByClass("tr-qtyAvailable").text();
                Integer qoh = qohParser(qtyAvailableRaw);

                Map<String, Field> map = new LinkedHashMap<>();
                map.put("dpn", Field.create(dpn));
                map.put("mpn", Field.create(mpn));
                map.put("mfg", Field.create(mfg));
                map.put("qoh", Field.create(qoh));

                record.set(Field.create(Field.Type.MAP, map));

                batchMaker.addRecord(record);

            }
        } catch (Exception e) {

            System.out.println(e.getMessage());

        }
    }

    Integer qohParser(String qtyAvailableRaw) {

        Integer qoh = 0;
        try {
            String pattern = "(\\d+) - Immediate.*";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(qtyAvailableRaw.replace(",", ""));
            m.find();

            qoh = Integer.valueOf(m.group(1));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return qoh;
    }

}